package www.chatapp.com.chatapplicationpoc.util;


public class URLs {
    public static final String ROOT_URL = "https://moodlynktestserver.000webhostapp.com/MoodlynkServer/v1/";

    public static final String URL_REGISTER = ROOT_URL + "register";
    public static final String URL_STORE_TOKEN = ROOT_URL + "storegcmtoken/";
    public static final String URL_FETCH_MESSAGES = ROOT_URL + "messages";
    public static final String URL_SEND_MESSAGE = ROOT_URL + "send";
}
